1) Student will select the electronic component from the listed symbols.<br>
2) Pick and place the electronic component  to make the circuit of half wave rectifier without filter. <br>
3) Select the wire to connect the electronic components .<br>
4) Click on <b>Connect</b> button . <br>
5) Input the values of given parameter. <br>
6) To calculate the ripple factor ask the student to enter a pre-assumed value and check it with correct value . <br>
7) Click on <b>Supply</b> button for output waveform. <br>
8) Now press <b>Reset</b> button ,then press the <b>Filter</b> toggle switch to activate the filter. <br>
9) Pick and place the electronic component  to make the circuit of half wave rectifier with filter. <br>
10) Select the wire to connect the electronic components.<br>
11) Click on <b>Supply</b>button to see output waveform for With filter <br>
12) Now compare the outcome waveform of half wave rectifier with and without filter. <br>

