The conversion of AC into DC is called Rectification. Electronic devices can convert AC power into DC power with high efficiency.<br>
Consider the given circuit and for the diode to be ideal (i.e. Vf  = 0,  Rr =, Rs  = 0 ) and for non- ideal case enter the values from input dropbox. During  the positive half cycle, the diode is in forward biased condition and hence a current flows through the load resistor. During the negative half cycle, the diode is reverse biased and it is equivalent to an open circuit, hence the current through the load resistance is zero and output will be nothing. Thus the diode conducts only for one half cycle and results in a half wave rectified output.<br>

###### RIPPLE FACTOR:
<br>
  V<sub>rms</sub> is the rms voltage and V<sub>m</sub> is input voltage
    V<sub>rms</sub>= V<sub>m</sub>/2  <br>

The output of a half – wave rectifier consists of some undesirable ac components known as ripple. These can be removed using suitable filter circuits.
Ripple factor is defined as the ratio of the effective value of AC components to the average DC value. It is denoted by the symbol Ƴ --<br>
Ƴ = V<sub>ac</sub>/V<sub>dc</sub><br>
V<sup>2</sup><sub>rms</sub> = V<sup>2</sup><sub>ac</sub> + V<sup>2</sup><sub>dc</sub><br>
Ƴ = √ (V<sup>2</sup><sub>rms</sub> - V<sup>2</sup><sub>dc</sub>)/ V<sup>2</sup><sub>dc</sub><br>
Converting V rms and Vdc into its corresponding Vm value, we get<br>
                       Ƴ=1.21
                       
###### Operation with capacitor :
During the positive half cycle, the diode is forward biased and the capacitor gets charged as well as the load gets supply. During negative half cycle the diode gets reverse biased and the circuit is open during which the capacitor supplies the stored energy in it. The more the energy storage capacity the lesser the ripple in the output waveform.<br>

The ripple factor can be calculated theoretically by, <br>
              Ƴ = 1/2&radic;3 *f*R<sub>L</sub>*C

